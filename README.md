# Schedule creator #

A simple Gantt-style chart creator

Uses [svg.js](https://github.com/wout/svg.js) for drawing.

Currently only uses localStorage, there is no backend.

Live demo at http://monomon.me/stuff/gantt/