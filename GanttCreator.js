/**
 * Gantt creator
 * author: Mois Moshev
 */

// some crap to load
var testData = 	[
	{"range":[1,5],"name":"framework"},
	{"range":[1,6],"name":"sprites"},
	{"range":[1,6],"name":"menus"},
	{"range":[3,8],"name":"music and sound"},
	{"range":[5,9],"name":"testing"},
	{"range":[8,12],"name":"balancing"},
	{"range":[12,16],"name":"release"},
	{"range":[16,20],"name":"afterparty"}
];


/**
 * strongly coupled to creator, but makes it easier to manage tasks
 */
var Task = {
	init : function (x, y, width, height, owner) {
		this.row = 0;
		// get range for model
		var rangeCoords = owner.toGrid([x, x+width]);
		this.range = owner.model.addRange(rangeCoords);

		var layer = owner.layers.boxGroup.group()
			.attr('class', 'newElement')
			.move(x + 0.5, y + 0.5);
		layer.rect(width + 0.5, height + 0.5);
		// hook rectangle double lcick
		layer.get(0).dblclick(owner.onBoxDblClick);

		// set references
		layer.instance = this;
		this.layer = layer;
		return this;
	},

	setName : function (name) {
		// get rect bbox
		var bbox = this.layer.get(0).bbox();
		if (name && this.layer) {
			var node = this.layer;
			this.range.setName(name);
			// update text element
			var txt = node.get(1);
			if (!txt) {
				// create
				txt = node.text(name);
				txt.move(bbox.x + bbox.width/2, bbox.y + bbox.height/2 - 12);
			} else {
				// update
				if (txt) {
					txt.text(name);
				}
			}
		}
	}
};

var GanttCreator = (function () {
	"use strict";

	var self = {};
	self.storageKey = 'garga';

	self.init = function (config) {
		self.svg = SVG(config.elID).size(config.width, config.height);
		self.grid = config.grid;
		self.resizeThreshold = config.resizeThreshold;
		self.storage = localStorage;
		// default to true
		self.liveUpdate = config.liveUpdate || true;

		self.model = Object.create(RangeCollection).init({
			distance : 1
		});

		if (!self.storage.getItem(self.storageKey)) {
			self.storage.setItem(self.storageKey, '[]');
		}

		self.dragInfo = null;
		self.initMenu();
		self.layers = {};
		self.drawGrid();
		// do this after the grid is setup so that boxes are on top
		self.layers.boxGroup = self.svg.group();

		SVG.on(self.svg.parent, 'mousedown', self.startDrag);

		// if data passed in config
		if (config.ranges) {
			self.loadRanges(config.ranges);
		}

		// if localStorage has been set
		if (self.storage.getItem(self.storageKey)) {
			self.loadRanges(JSON.parse(self.storage.getItem(self.storageKey)));
		}

		return self;
	};

	self.drawGrid = function () {
		var gridGroup = self.layers.gridGroup = self.svg.group();
		self.layers.gridNumbersGroup = self.svg.group();

		var bbox = self.svg.viewbox();
		var gridOffset = [5, 20];
		var numCols = Math.floor(bbox.width/self.grid.x) - 1;
		var numRows = Math.floor(bbox.height/self.grid.y) -1;

		gridGroup.move(gridOffset[0], gridOffset[1]);
		self.layers.gridNumbersGroup.move(gridOffset[0], 0);

		// offset by 0.5 so line is in the middle
		// vertical lines
		for (var i=0; i<=numCols; i ++) {
			var txt = self.layers.gridNumbersGroup.text(i.toString());
			txt.addClass('grid-text').move(i*self.grid.x, 0);
			gridGroup.line(i*self.grid.x+0.5, 0, i*self.grid.x+0.5, numRows*self.grid.x + 1)
			.stroke({
				width : 1,
				color : '#bababa',
				dasharray : '4,2'
			});
		}

		// horizontal lines
		for (i=0; i<=numRows; i ++) {
			gridGroup.line(0, i*self.grid.y+0.5, numCols*self.grid.y, i*self.grid.y+0.5)
			.stroke({
				width : 1,
				color : '#999'
			});
		}
		// set bbox on a property for easy lookup
		self.gridBox = gridGroup.bbox();
		return gridGroup;
	};

	self.initMenu = function () {
		self.menuEl = document.getElementById('menu');
		self.menuEl.querySelector('.clear').addEventListener('click', self.onClear);
		self.menuEl.querySelector('.save').addEventListener('click', self.onSave);
		self.menuEl.querySelector('.getJSON').addEventListener('click', function (evt) {
			var el = document.getElementById('json');
			el.firstChild.innerHTML = self.serializeModel(self.model);
			el.style.display = 'block';
			evt.preventDefault();
		});
		self.menuEl.querySelector('.loadLocal').addEventListener('click', function (evt) {
			self.onClear();
			self.loadRanges(JSON.parse(self.storage.getItem(self.storageKey)));
			evt.preventDefault();
		});
		self.menuEl.querySelector('.loadTest').addEventListener('click', function (evt) {
			self.onClear();
			self.loadRanges(testData);
			evt.preventDefault();
		});
		self.menuEl.querySelector('.usage').addEventListener('click', function (evt) {
			alert('* drag in grid to create events\n* resize events by dragging their edges\n* double click to set event name');
			evt.preventDefault();
		});
		document.querySelector('#json .hide').addEventListener('click', function (evt) {
			evt.target.parentNode.style.display = 'none';
			evt.preventDefault();
		});
	};

	self.updateJSON = function () {
		document.querySelector('#json pre').innerHTML = self.serializeModel(self.model);
	};

	self.startDrag = function (evt) {
		if (!evt.target && !evt.target.instance) {
			return;
		}

		// setup drag info
		self.dragInfo = {};
		self.dragInfo.startEvent = evt;
		evt.preventDefault();
		// hook listeners
		SVG.on(self.svg.parent, 'mousemove', self.moveDrag);
		SVG.on(self.svg.parent, 'mouseup', self.endDrag);

		// if an existing node selected
		var mouseCoords = self.screenToLocal([evt.pageX, evt.pageY]);
		var snappedLocation = self.localToScreen(self.snapToGrid(mouseCoords));

		// if a node was selected
		if (evt.target.instance.parent.type == 'g' &&
			evt.target.instance.parent.hasClass('entry')) {

			self.dragInfo.selectedNode = evt.target.instance.parent;
			self.dragInfo.selectedNode.addClass('selected');
			var rect = self.dragInfo.selectedNode.get(0);

			// TODO: make a translation function for this
			self.dragInfo.selectionOffset = [
				mouseCoords[0] - self.dragInfo.selectedNode.x(),
				mouseCoords[1] - self.dragInfo.selectedNode.y()
			];

			// whether to resize
			var rectWidth = rect.width();
			if (Math.abs(self.dragInfo.selectionOffset[0]) < self.resizeThreshold) {
				self.dragInfo.resizeHandle = 0;
				self.dragInfo.originalWidth = rectWidth;
				self.dragInfo.selectedNode.addClass('resizing');
			} else if (Math.abs(rectWidth - self.dragInfo.selectionOffset[0]) < self.resizeThreshold) {
				self.dragInfo.resizeHandle = 1;
				self.dragInfo.originalWidth = rectWidth;
				self.dragInfo.selectedNode.addClass('resizing');
			} else {
				self.dragInfo.resizeHandle = null;
			}
		} else {
			// create a new one
			self.dragInfo.newShape = self.addNode(
				snappedLocation[0], snappedLocation[1], 0, self.grid.y).layer;
		}
	};

	self.moveDrag = function (evt) {
		if (!self.dragInfo) {
			return;
		}

		evt.preventDefault();

		// subtract position of svg element to get local coordinates
		var newLoc = self.localToScreen(self.snapToGrid(self.screenToLocal([
			evt.pageX,
			evt.pageY
		])));

		var oldLoc = self.localToScreen(self.snapToGrid(self.screenToLocal([
			self.dragInfo.startEvent.pageX,
			self.dragInfo.startEvent.pageY
		])));

		var rect;

		if (self.dragInfo.selectedNode) {
			// either resizing or moving
			var dragOffset = self.snapToGrid([
				self.dragInfo.selectionOffset[0],
				self.dragInfo.selectionOffset[1]
			]);
			var node = self.dragInfo.selectedNode;
			rect = node.get(0);
			var text = node.get(1);
			var bbox = rect.bbox();

			// also pass rect since we already obtained it
			if (self.dragInfo.resizeHandle === 0) {
				self.resizeLeft(
					node,
					rect,
					self.dragInfo.originalWidth,
					newLoc,
					oldLoc
				);
			} else if (self.dragInfo.resizeHandle === 1) {
				self.resizeRight(
					node,
					rect,
					self.dragInfo.originalWidth,
					newLoc,
					oldLoc
				);
			} else {
				self.onNodeMove(
					node,
					newLoc,
					dragOffset
				);
			}

			return;
		}

		// TODO: calculate delta and use it for all operations
		var newShape = self.dragInfo.newShape;
		if (newShape) {
			rect = newShape.get(0);
			// going to the right
			if (newLoc[0] > oldLoc[0]) {
				rect.width(newLoc[0] - oldLoc[0]);
				// TODO: subtract 1 to get an inclusive range? That would require care though
				newShape.instance.range.setEnd(Math.round((newShape.x() + rect.width())/self.grid.x));
			// going to the left
			} else {
				newShape.x(newLoc[0]);
				rect.width(oldLoc[0] - newLoc[0]);
				newShape.instance.range.setStart(Math.floor(newShape.x()/self.grid.x));
			}
		}
	};

	self.endDrag = function (evt) {
		if (self.dragInfo.selectedNode) {
			var node = self.dragInfo.selectedNode;
			node.removeClass('selected');
			node.removeClass('resizing');

			var bbox = node.get(0).bbox();
			if (bbox.width <= 0.5) {
				self.removeNode(node);
			}
		} else if (self.dragInfo.newShape) {
			var rect = self.dragInfo.newShape.get(0);
			if (rect.width() <= 0.5) {
				self.dragInfo.newShape.clear();
				self.dragInfo.newShape.remove();
			} else {
				self.dragInfo.newShape.removeClass('newElement');
				self.dragInfo.newShape.addClass('entry');
			}
		}

		self.dragInfo = null;
		SVG.off(self.svg.parent, 'mousemove', self.moveDrag);
		SVG.off(self.svg.parent, 'mouseup', self.endDrag);
	};

	self.onNodeMove = function (node, newLoc, dragOffset) {
		// moving
		var delta = [
			newLoc[0] - dragOffset[0],
			newLoc[1] - dragOffset[1]
		];
		node.move(delta[0], delta[1]);
		// update range
		var newRange = self.toGrid(delta);
		// set end first so that length is still unchanged
		// simply easier...
		node.instance.range.setEnd(newRange[0] + node.instance.range.getLength());
		node.instance.range.setStart(newRange[0]);
		node.instance.row = newRange[1];

		if (self.liveUpdate)
			self.updateJSON();
	};

	self.removeNode = function(node) {
		// remove range from model
		self.model.removeRange(node.instance.range);
		// delete instance
		delete node.instance;
		// remove child nodes
		node.clear();
		node.remove();

		if (self.liveUpdate)
			self.updateJSON();
	};

	self.onClear = function(evt) {
		self.model.clear();
		self.layers.boxGroup.clear();
		if (evt && evt.preventDefault)
			evt.preventDefault();

		if (self.liveUpdate)
			self.updateJSON();
	};

	/**
	 * Save button handler
	 * fixme
	 * save through model?
	 * serialize ranges
	 * use localStorage at first
	 * save screen coordinates too?
	 */
	self.onSave = function(evt) {
		self.storage.setItem('garga', self.serializeModel(self.model));
		evt.preventDefault();
	};

	/**
	 * serialize model for saving
	 */
	self.serializeModel = function (model) {
		var map = model.ranges.map(function (element) {
			var e = {
				range : element.values,
				name : element.name
			};

			return e;
		});

		return JSON.stringify(map, undefined, 2);
	};

	/**
	 * load ranges from an array
	 */
	self.loadRanges = function(ranges) {
		for (var i=0, len=ranges.length; i<len; i++) {

			if(!ranges[i].range) {
				console.log("invalid range passed");
				continue;
			}

			var coords = self.fromGrid(ranges[i].range);
			var node = self.addNode(
				coords[0] + self.layers.gridGroup.x(),
				// offset from the start so that menu does not overlap
				((ranges[i].row || i+2))*self.grid.y + self.layers.gridGroup.y(),
				coords[1]-coords[0],
				self.grid.y
			).layer
			.removeClass('newElement')
			.addClass('entry');

			if (ranges[i].name) {
				self.setNodeName(node, ranges[i].name);
			}
		}
	};

	/**
	 * create a new box and add it to the layers
	 * also initialize a range in the model
	 * perhaps this should work by passing a range instead
	 */
	self.addNode = function(x, y, width, height) {
		var node = Object.create(Task).init(x, y, width, height, self);

		if (self.liveUpdate)
			self.updateJSON();

		return node;
	};

	self.resizeLeft = function (node, rect, originalWidth, newLoc, oldLoc) {
		node.x(newLoc[0]);
		var newWidth = originalWidth - newLoc[0] + oldLoc[0];
		rect.width(newWidth);
		// update model range
		node.instance.range.setStart(Math.floor(node.x()/self.grid.x));
		self.onResize(node, rect);
	};

	self.resizeRight = function (node, rect, originalWidth, newLoc, oldLoc) {
		var newWidth = self.dragInfo.originalWidth + newLoc[0] - oldLoc[0];
		rect.width(newWidth);
		// update model range
		node.instance.range.setEnd(Math.round((node.x() + newWidth)/self.grid.x));
		self.onResize(node, rect);
	};

	/**
	 * called once a node has been resized
	 */
	self.onResize = function (node, rect) {
		// update text, maybe do other things
		var bbox = rect.bbox();

		var text = node.get(1);
		if (text) {
			text.x(bbox.x + bbox.width/2);
		}

		if (self.liveUpdate)
			self.updateJSON();
	};

	self.onBoxDblClick = function (evt) {
		var name = prompt('task name', evt.target.instance.parent.instance.range.getName());
		self.setNodeName(evt.target.instance.parent, name);
	};

	self.setNodeName = function (node, name) {
		node.instance.setName(name);

		if (self.liveUpdate)
			self.updateJSON();
	};

	// move utility functions elsewhere?
	self.fromGrid = function (coords) {
		return [
			coords[0]*self.grid.x,
			coords[1]*self.grid.y
		];
	};

	self.snapToGrid = function (coords) {
		return self.fromGrid(self.toGrid(coords));
	};

	self.toGrid = function (coords) {
		return [
			Math.round(coords[0]/this.grid.x),
			Math.floor(coords[1]/this.grid.y)
		];
	};

	// these functons are slightly different; think of a better name
	self.screenToLocal = function (coords) {
		return [
			coords[0] - self.svg.parent.offsetLeft - self.gridBox.x,
			coords[1] - self.svg.parent.offsetTop - self.gridBox.y
		];
	};
	// here screen coordinates are relative to svg el coordinates
	self.localToScreen = function (coords) {
		return [
			coords[0] + self.gridBox.x,
			coords[1] + self.gridBox.y
		];
	};

	return self;
}());