/**
 * Range collection implementation
 * author: Mois Moshev
 */

var LEFT = 0, RIGHT = 1;
// types of intersections
var intersect = {
	INSIDE : 0,
	ATBORDER : 1,
	WITHINDIST : 2,
	OUTSIDE : 3
};

/*
Single range of values
Has a lower and upper value, stored in an array
*/
var Range = {
	values : undefined,
	name : undefined,

	init : function(v1, v2) {
		this.values = v1 ? [v1, v2 || v1] : [];
		return this;
	},

	getStart : function() {
		return this.values[LEFT];
	},

	setStart : function(value) {
		this.values[LEFT] = value;
	},

	getEnd : function() {
		return this.values[RIGHT];
	},

	setEnd : function(value) {
		this.values[RIGHT] = value;
	},

	setValue : function(value, direction) {
		this.values[direction] = value;
	},

	getLength : function() {
		return this.values[RIGHT] - this.values[LEFT];
	},

	setName : function(name) {
		this.name = name;
	},

	getName : function(name) {
		return this.name;
	}
};

/**
 * Contains an array of value ranges and some methods for operating
 * with them
 */
var RangeCollection = {
	ranges : [],
	distance : undefined, // cutoff distance

	/*
	simple case of initialization
	initialize ranges from sorted values
	*/
	init : function(options) {
		this.ranges = [];
		this.distance = options.distance;

		// return if no data is passed
		if (!options.values || options.values.length <= 0) {
			return this;
		}

		var values = options.values;

		// start first range to make things simpler
		this.ranges.push(Object.create(Range).init(values[0]));

		// keep track of the latest range
		var latestRange = this.ranges[0];
		var i, len=values.length;

		// go over the inner values of the array
		for(i=1; i<len; i++) {
			var value = values[i];

			if(this.withinDistance(value, values[i-1])) {
				latestRange.setEnd(value);
			} else {
				// cut last range and start a new one
				latestRange.setEnd(values[i-1]);
				var newRange = Object.create(Range).init(value);
				this.ranges.push(newRange);
				latestRange = newRange;
			}
		}

		return this;
	},

	/*
	process a single value
	if inside a range, remove it from there
	if not, add it
	*/
	processValue : function(value) {
		var range;
		var intersection;
		value = Number(value);
		// start search in the middle
		var index = Math.floor(this.ranges.length/2);

		if (this.ranges.length === 0) {
			this.ranges.push(Object.create(Range).init(value));
			return;
		}

		// find where to insert value
		// FIXME: start from last inserted
		while(index>=0 && index<this.ranges.length) {
			range = this.ranges[index];
			intersection = this.intersectsRange(value, range);
			switch(intersection[0]){

				case intersect.INSIDE:
					// split range in two
					this.splitRange(range, index, value);
					return;

				case intersect.ATBORDER:
					// shorten range and remove if it will become zero length
					if(range.getStart() - range.getEnd() === 0) {
						this.removeRange(range, index);
					} else {
						this.ranges[index].setValue(value - this.getDirectionVector(intersection[1])*this.distance, intersection[1]);
					}
					return;

				case intersect.WITHINDIST:
					// update with new value
					range.setValue(value, intersection[1]);

					// find neighbour depending on intersecting side
					var neighbourIndex = this.getNeighbourIndex(index, intersection[1]);
					var neighbour = this.ranges[neighbourIndex];

					// check if merge is needed
					if(neighbour && this.withinDistance(range.values[intersection[1]], neighbour.values[this.reverseDirection(intersection[1])])) {

						this.mergeRanges([range, neighbour], [index, neighbourIndex], intersection[1]);
					}
					return;

				case intersect.OUTSIDE:
					// sorted insert of the value as a range
					var neighbourIndex = this.getNeighbourIndex(index, intersection[1]);
					var neighbour = this.ranges[neighbourIndex];

					// if next neighbour outside in the other direction - insert this value as a new range
					var neighbourIntersect = this.intersectsRange(value, neighbour);
					var newRange = Object.create(Range).init(value);

					if(neighbour && neighbourIntersect[0] === intersect.OUTSIDE && neighbourIntersect[1] !== intersection[1]) {
						// need to insert new range in the middle
						this.ranges.splice(Math.max(index, neighbourIndex), 0, newRange);
						return;
					}

					// console.log(index, neighbourIndex);
					if (neighbourIndex < 0) {
						this.ranges.unshift(newRange);
						return;
					} else if (neighbourIndex >= this.ranges.length) {
						this.ranges.push(newRange);
						return;
					}

					index = neighbourIndex;
					// else continue with updated index
					break;
			}
		}
	},

	addRange : function(range, name) {
		// find where it fits and how it intersects others
		// this version does not check intersections because it is not needed. Check the strange phenomena implementation
		var newRange = Object.create(Range).init(range[0], range[1]);
		if (name) {
			newRange.setName(name);
		}
		this.ranges.push(newRange);
		// return last index
		return newRange;
	},

	removeValue : function(value) {
		// need to cut if inside
	},

	removeRange : function(range, index) {
		if (index === undefined) {
			index = this.ranges.indexOf(range);
		}
		this.ranges.splice(index, 1);
	},

	mergeRanges : function(ranges, indexes, direction) {
		// extend first range in the given direction
		ranges[0].setValue(ranges[1].values[direction], direction);
		// remove swallowed range
		this.ranges.splice(indexes[1], 1);
	},

	setRanges : function(ranges) {
		this.ranges = ranges;
	},

	sortRanges : function() {

	},

	/*
	split range into two
	insert another range into array
	lower range ends one day before value
	higher range starts one day after value
	*/
	splitRange : function(range, index, value) {
		// this will be the 'upper' range
		var newRange = Object.create(Range).init(value + this.distance, range.getEnd());

		// set lower range value
		this.ranges[index].setEnd(value - this.distance);

		// insert new range
		if(index<this.ranges.length-1) {
			this.ranges.splice(index+1, 0, newRange);
		} else {
			this.ranges.push(newRange);
		}

		return [this.ranges[index], newRange];
	},

	withinDistance : function(v1, v2) {
		return (Math.abs(v1-v2) <= this.distance);
	},

	/*
	check how a range intersects with a single value
	return an intersection type and direction
	(on which side of the range the new value is)
	*/
	intersectsRange : function(value, range) {
		if(!range) {
			return false;
		}

		// get values for comparison
		var start = range.getStart();
		var end = range.getEnd();
		value = value.valueOf();

		if(value === start) {
			return [intersect.ATBORDER, LEFT];
		}
		if(value === end) {
			return [intersect.ATBORDER, RIGHT];
		}
		if(value < start) {
			if(this.withinDistance(value, start)) {
				return [intersect.WITHINDIST, LEFT];
			} else {
				return [intersect.OUTSIDE, LEFT];
			}
		} else if(value > end) {
			if(this.withinDistance(value, end)) {
				return [intersect.WITHINDIST, RIGHT];
			} else {
				return [intersect.OUTSIDE, RIGHT];
			}
		} else {
			return [intersect.INSIDE];
		}
	},

	// check how two ranges intersect
	rangeIntersectsRange : function (range, otherRange) {
		return [
			this.intersectsRange(range[0], otherRange),
			this.intersectsRange(range[1], otherRange)
		];
	},

	// get index of neighbour in the direction pointed
	getNeighbourIndex : function(index, direction) {
		return (index + this.getDirectionVector(direction));
	},

	// get vector in the given direction
	getDirectionVector : function(direction) {
		return direction - this.reverseDirection(direction);
	},

	reverseDirection : function(direction) {
		return Math.abs(direction - 1);
	},

	clear : function() {
		this.ranges = [];
	},

	/*
	Spit out all values from all ranges in an array
	FIXME: not abstract because of getTime! Find another way to do this
	*/
	rangesToValues : function() {
		var values = [];
		// TODO: how to make this generic and avoid using getTime?
		for(var i=0, len=this.ranges.length; i<len; i++) {
			for(var start=this.ranges[i].getStart(), end=this.ranges[i].getEnd(); start<=end; start += this.distance) {
				values.push(start);
			}
		}

		return values;
	}
};
